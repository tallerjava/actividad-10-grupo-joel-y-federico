
import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        ServicioReporte sReporte = new ServicioReporte();
        sReporte.gestiomarReporte("C:\\Users\\Joe\\Documents\\NetBeansProjects\\actividad-10-grupo-joel-y-federico\\donaciones.txt");
        Reporte reporte = sReporte.calcular();
        reporte.ordenarPorPrecio();
        System.out.println("=Paises======Montos===");
        for (Donacion e : reporte.getDonacionesPorPais()) {

            System.out.println(e.getPais() + " " + e.getMonto());
        }

        System.out.println("Total General: " + reporte.getTotal());
    }
}
