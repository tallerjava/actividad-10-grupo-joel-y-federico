
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reporte {

    private double total;
    private List<Donacion> donacionesPorPais = new ArrayList();

    public Reporte(double total, HashMap<String, Double> donacionescalculadas) {
        this.total = total;
        for (Map.Entry<String, Double> donacion : donacionescalculadas.entrySet()) {
            donacionesPorPais.add(new Donacion(donacion.getKey(), donacion.getValue()));
        }
        this.total = total;
    }

    public List<Donacion> getDonacionesPorPais() {
        return donacionesPorPais;
    }

    public double getTotal() {
        return total;
    }

    public void ordenarPorPrecio() {
        Collections.sort(donacionesPorPais);
    }
}
