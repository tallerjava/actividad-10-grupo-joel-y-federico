
import java.io.*;
import java.util.ArrayList;

public class Repositorio {

    private ArrayList<Donacion> lista = new ArrayList<>();

    public void cargarArchivo(String rutaArchivo) throws IOException {
        FileReader fichero = new FileReader(rutaArchivo);
        /* Se crea un fichero de lectura*/
        BufferedReader br = new BufferedReader(fichero);
        /* Se crea un "lector" de ficheros*/
        try {
            String linea;
            String paisTemp;
            String montoTemp;
            while ((linea = br.readLine()) != null) {
                paisTemp = linea.split("\\|")[1];
                montoTemp = linea.split("\\|")[6];
                montoTemp = montoTemp.replace("$", "");
                if (!montoTemp.equals("donacion")) {
                    Double flotante = Double.parseDouble(montoTemp);
                    lista.add(new Donacion(paisTemp, flotante));
                }
            }
        } finally {
            if (br != null) {
                br.close();
            }
        }
    }

    public ArrayList<Donacion> getLista() {
        return lista;
    }
}
